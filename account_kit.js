define(function(){
	var FBAccountKit = {
		// LOAD
			init : function(settings){
				if(document.accountKitPromise) return document.accountKitPromise;

				document.accountKitPromise = new Promise(function(resolve, reject){
					AccountKit.init({
						appId 		: settings.facebookID,
						state 		: '796cfe3d60ae31fc08eb879c105109a9',
						version 	: 'v1.0'
					});

					return resolve();
				});

				return document.accountKitPromise;
			},

		// LOGIN
			login : function(method, params){
				return new Promise(function(resolve, reject){
					FBAccountKit.init().then(function(){
						AccountKit.login(method, params, function(result){
							if(result.status === 'PARTIALLY_AUTHENTICATED') return resolve(result.code);
							else return reject(result);
						});
					}, reject);
				});
			}
	};

	return FBAccountKit;
});